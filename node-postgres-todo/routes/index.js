var express = require('express');
var router = express.Router();
var path = require('path');
var pg = require('pg');
var connectionString = process.env.DATABASE_URL || 'postgres:postgres:123@localhost:5432/todo';

router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../views', 'index.html'));
});

/* POST methods */
router.post('/api/v1/todos', function(req, res) {
    var results = [];
    var data = {text: req.body.text, complete: false};
    pg.connect(connectionString, function(err, client, done) {
        if(err) {
          done();
          console.log(err);
          return res.status(500).json({ success: false, data: err});
        }
        client.query("INSERT INTO items(text, complete) values($1, $2)", [data.text, data.complete]);
        var query = client.query("SELECT * FROM items ORDER BY id ASC");
        query.on('row', function(row) {
            results.push(row);
        });
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });
});


router.get('/api/v1/todos', function(req, res) {
    var results = [];
    pg.connect(connectionString, function(err, client, done) {
        if(err) {
          done();
          console.log(err);
          return res.status(500).json({ success: false, data: err});
        }
        var query = client.query("SELECT * FROM items ORDER BY id ASC;");
        query.on('row', function(row) {
            results.push(row);
        });
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });
});

router.put('/api/v1/todos/:todos_id',function(req, res){
  var results = [];
  var id = req.params.todos_id;
  var data = {text: req.body.text, complete: req.body.complete};
  console.log(req.body.text);
  console.log(req.body.complete);
  pg.connect(connectionString, function(err, client, done){
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    var sql = client.query("UPDATE items SET text= ($1), complete = ($2) where id=($3)",[data.text, data.complete, id]);
    console.log(sql)
    var query = client.query("SELECT * FROM items ORDER BY id ASC;");
    query.on('row', function(row) {
        results.push(row);
    });
    query.on('end', function(){
      done();
      return res.json(results);
    });
  });
});

router.delete('/api/v1/todos/:todos_id',function(req, res){
  var results = [];
  var id = req.params.todos_id;
  var data = {text: req.body.text, complete: req.body.complete};
  console.log(req.body.text);
  console.log(req.body.complete);
  pg.connect(connectionString, function(err, client, done){
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    var sql = client.query("DELETE FROM items WHERE id=($1)", [id]);
    console.log(sql)
    var query = client.query("SELECT * FROM items ORDER BY id ASC;");
    query.on('row', function(row) {
        results.push(row);
    });
    query.on('end', function(){
      done();
      return res.json(results);
    });
  });
});

module.exports = router;
